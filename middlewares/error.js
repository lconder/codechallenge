module.exports =  (err, req, res, next) => {
  console.error('error middleware');
  console.error(err);
  if (err.isJoi) {
    return res.sendStatus(400);
  }
  res.sendStatus(500);
};
