/**
 * @swagger
 * tags:
 *   name: CSV
 *   description: csv
 */

/**
 *  @swagger
 *  /:
 *  post:
 *    description: Upload CSV file
 *    tags: [CSV]
 *    requestBody:
 *      required: true
 *      content:
 *        multipart/form-data:
 *          schema:
 *            type: object
 *            properties:
 *              file:
 *                type: file
 *                format: csv
 *                description: Archivo a subir
 *    responses:
 *      '200':
 *        description: Success
 *      '500':
 *        description: Internal Server Error
 */
