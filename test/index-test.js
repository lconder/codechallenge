const chai = require('chai');
const chaiHttp = require('chai-http');
const assert = require('assert');
const should = chai.should();
const Car = require('../models/car');
const fs = require('fs');
chai.use(chaiHttp);
const app = require('../app.js');

const car_data = {
  UUID : "100",
  VIN : "0SG4zAX",
  make : "WS",
  year : 1955,
  mileage : 118,
  model : "CE",
  price : 6740.65,
  zipcode : "38945",
  created_at : "1948-03-26",
  updated_at : "2021-06-02"
}

describe('Index', function() {
  describe('Car', function() {
    it('Creating a new Car', async function() {
      const car = await new Car(car_data);
      car.save();
      car.should.to.have.property('_id');
      car.should.to.have.property('id');
      car.should.to.have.property('UUID');
      car.should.to.have.property('VIN');
      car.should.to.have.property('make');
      car.should.to.have.property('year');
      car.should.to.have.property('mileage');
      car.should.to.have.property('model');
      car.should.to.have.property('price');
      car.should.to.have.property('zipcode');
      car.should.to.have.property('created_at');
      car.should.to.have.property('updated_at');
    });
  });
  describe('HTTP Requests', function() {
    it('POST', async () => {
      const response = await chai
        .request(app)
        .post(
          '/'
        )
        .set('Content-Type', 'multipart/form-data')
        .attach(
          'file',
          fs.readFileSync(`${process.cwd()}/test/csvToTest.csv`),
          'csvToTest.csv'
        );
      response.should.have.status(200);
    });
  });
});
