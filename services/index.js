const Busboy = require('busboy');
const csv = require('csv-parser');
const Car = require('../models/car');

function streamFile(payload) {
  try {
    const { headers } = payload;

    const busboy = new Busboy({ headers });
    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
      file.pipe(csv())
        .on('data', function (data) {
          Car.create(data);
        });
    });
    busboy.on('finish', function() {
      console.log('Done');
    });
    payload.pipe(busboy);
  } catch (e) {
    console.error(e);
    throw new Error(e);
  }
}

module.exports = {
  streamFile,
}
