const services = require('../services/index')

async function create(req, res, next) {
  try {
    services.streamFile(req);
    res.sendStatus(200);
  } catch (e) {
    next(e);
  }
}

async function get(req, res, next) {
  try {
    res.send('Hello');
  } catch (e) {
    next(e);
  }
}

module.exports = {
  create,
  get,
}
