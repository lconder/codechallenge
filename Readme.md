# CSV Upload

- Clone the repository (You need MongoDB Database)
- Run: npm install
- Run: npm start
- To test: npm test

## Observations

- I wanted to implement Joi validation, but I had a few problems, so I decided to skipped.
- I had no time to implements MongoDB Memory Server
- I wrote some tests, but it can be enhanced
- In another scenario I would not upload my .env
