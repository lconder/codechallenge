const express = require('express');
const path = require('path');
const fs = require('fs-extra');
const cors = require('cors');
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');
const app = express();
const PORT = require('./controllers/keys').PORT || 3000;

const options = {
  swaggerDefinition: {
    openapi: '3.0.0',
    info: {
      title: 'API',
      version: '1.0.0',
      description: 'Sample API to load CSV Files',
    },
    servers: [
      {
        url: `http://localhost:${PORT}/`,
      },
    ],
  },
  apis: ['app.js', './models/*.js', './docs/*.js'],
};
const specs = swaggerJsDoc(options);
app.use('/docs', swaggerUI.serve);
app.get('/docs', async (req, res, next) => {return next();
}, swaggerUI.setup(specs));

const uploadPath = path.join(__dirname, 'uploads/');
fs.ensureDir(uploadPath);

require('./routes/index')(app);
require('./startup/db')();

app.use(cors());

app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));

app.use(require('./middlewares/error'));
// To test:
module.exports = app;
