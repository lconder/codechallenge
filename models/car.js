const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const carSchema = new Schema({
    UUID: { type: String },
    VIN: { type: String },
    make: { type: String },
    model: { type: String },
    mileage: { type: Number },
    year: { type: Number },
    price: { type: Number },
    zipcode: { type: String },

    created_at: {type: Date, select: false, default: Date.now},
    updated_at: {type: Date, select: false, default: Date.now}
  },
  { versionKey: false });

carSchema.pre('save', next => {
  this.updated_at = new Date();
  next();
});

carSchema .set('toObject', {getters: true});
carSchema .set('toJSON', {getters: true});

module.exports = mongoose.model('Car', carSchema);
