module.exports = (app) => {
  const express = require('express');
  const api_routes = express.Router();

  const {
    create,
    get,
  } = require('../controllers/index');

  api_routes.get('/', get);

  api_routes.post(
    '/',
    create
  );

  app.use('/', api_routes);
};
